# -*- coding: utf-8 -*-
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions
from django.core.exceptions import ValidationError


class LoginForm(forms.Form):
    username = forms.CharField(
        required = True,
        label = ''
    )
    password = forms.CharField(
        required = True,
        label = ''
    )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.help_text_inline = True
        self.helper.form_class = 'form-signin'
        add_btn = Submit('btnsubmit', 'Login', css_class='btn btn-primary')
        self.helper.layout = Layout(
            Div(Field('username'), css_class="col-username"),
            Div(Field('password'), css_class="col-password"),
            Div(add_btn, css_class="col-button")
        )
        super(LoginForm, self).__init__(*args, **kwargs)
